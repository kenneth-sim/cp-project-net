﻿using cp_api_net.Data;
using cp_api_net.Models.Dto;
using cp_api_net.Models.Entities;
using cp_api_net.Services.Implementation.Clients;
using cp_api_net.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using System.Collections;
using System.Reflection;

namespace cp_api_net.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AnnouncementController : ControllerBase
    {
        private readonly IAnnouncementService _announcementService;
        private readonly CoinMarketCapClient _coinMarketCapClient;
        private readonly AppDbContext _db;
        private readonly IDistributedCache _distributedCache;

        public AnnouncementController(IAnnouncementService announcementService, CoinMarketCapClient coinMarketCapClient, AppDbContext db, IDistributedCache distributedCache)
        {
            _announcementService = announcementService;
            _coinMarketCapClient = coinMarketCapClient;
            _db = db;
            _distributedCache = distributedCache;
        }

        [HttpGet]
        public async Task<IActionResult> GetAnnouncementsAsync()
        {
            return Ok(new Response(await _announcementService.GetAnnouncements()));
        }

        [HttpGet("valid")]
        public async Task<IActionResult> GetValidAnnouncementsAsync()
        {
            return Ok(new Response(await _announcementService.GetValidAnnouncements()));
        }

        [HttpPost, Authorize(Roles = "ROLE_ADMIN")]
        public IActionResult AddAnnouncement([FromBody] Announcement announcement)
        {
            _announcementService.AddAnnouncement(announcement);
            return Ok(new Response("Announcement added successfully"));
        }

        [HttpPatch, Authorize(Roles = "ROLE_ADMIN")]
        public IActionResult updateAnnouncement([FromBody] Announcement announcement)
        {
            _announcementService.UpdateAnnouncement(announcement);
            return Ok(new Response("Announcement updated successfully"));
        }

        [HttpDelete("{id}"), Authorize(Roles = "ROLE_ADMIN")]
        public IActionResult deleteAnnouncement(int id)
        {
            _announcementService.DeleteAnnouncement(id);
            return Ok(new Response("Announcement removed successfully"));
        }

        [HttpGet("testing")]
        public async Task<IActionResult> testing()
        {
            string className = "cp_api_net.Services.Implementation.AnnouncementServiceImpl";
            Type type = Type.GetType(className);
            if (type != null)
            {
                // Get the constructor of the class
                ConstructorInfo constructor = type.GetConstructor(new[] { typeof(IDistributedCache), typeof(AppDbContext) });
                if (constructor != null)
                {
                    // Pass the required parameters to the constructor
                    object instance = constructor.Invoke(new object[] { _distributedCache, _db });

                    // Assuming the class has a method named "GetAnnouncements"
                    MethodInfo methodInfo = type.GetMethod("GetAnnouncements");

                    if (methodInfo != null)
                    {
                        // Invoke the method asynchronously
                        Task resultTask = (Task)methodInfo.Invoke(instance, null);
                        await resultTask.ConfigureAwait(false);

                        // Get the result using reflection
                        PropertyInfo resultProperty = resultTask.GetType().GetProperty("Result");
                        if (resultProperty != null)
                        {
                            object result = resultProperty.GetValue(resultTask);
                            return Ok(result);
                        }
                        else
                        {
                            return BadRequest("Result property not found.");
                        }
                    }
                    else
                    {
                        return BadRequest("Method not found.");
                    }
                }
                else
                {
                    return BadRequest("Constructor not found.");
                }
            }
            else
            {
                return BadRequest("Class not found.");
            }
        }
    }
}
