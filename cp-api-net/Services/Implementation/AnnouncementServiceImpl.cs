﻿using cp_api_net.Data;
using cp_api_net.Models.Entities;
using cp_api_net.Models.Error;
using cp_api_net.Services.Interfaces;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.IdentityModel.Tokens;

namespace cp_api_net.Services.Implementation
{
    public class AnnouncementServiceImpl : BaseServiceImpl, IAnnouncementService
    {
        private readonly AppDbContext _db;
        public AnnouncementServiceImpl(IDistributedCache distributedCache, AppDbContext db) : base(distributedCache, db)
        {
            _db = db;
        }

        public void AddAnnouncement(Announcement newAnnouncement)
        {
            var retvAnnouncement = _db.Announcements.FirstOrDefault(a => a.Title.Equals(newAnnouncement.Title));

            if (retvAnnouncement != null)
            {
                throw new ErrorCodeException(ErrorCode.EXISTED_ANNOUNCEMENT);
            }
 
            _db.Announcements.Add(newAnnouncement);
            _db.SaveChanges();
        }

        public async Task<IEnumerable<Announcement>> GetAnnouncements()
        {
            IEnumerable<Announcement>? announcements = await RetrieveDataFromRedis<IEnumerable<Announcement>>("Announcement");

            if(announcements.IsNullOrEmpty())
            {
                IEnumerable<Announcement>  retvAnnouncements = _db.Announcements.ToList();
                SaveDataToRedis("Announcement", retvAnnouncements);
                return retvAnnouncements;
            }
            else
            {
                return announcements!;
            }
        }

        public void DeleteAnnouncement(int id)
        {
            var retvAnnouncement = _db.Announcements.FirstOrDefault(a => a.Id.Equals(id));

            if (retvAnnouncement == null)
            {
                throw new ErrorCodeException(ErrorCode.NOTFOUND_ANNOUNCEMENT);
            }

            _db.Announcements.Remove(retvAnnouncement);
            _db.SaveChanges();
        }

        public void UpdateAnnouncement(Announcement announcement)
        {
            var retvAnnouncement = _db.Announcements.FirstOrDefault(a => a.Id.Equals(announcement.Id) || a.Title.Equals(announcement.Title));

            if (retvAnnouncement == null)
            {
                throw new ErrorCodeException(ErrorCode.NOTFOUND_ANNOUNCEMENT);
            }

            _db.Announcements.Remove(retvAnnouncement);
            _db.SaveChanges();
        }

        public async Task<IEnumerable<Announcement>> GetValidAnnouncements()
        {
            IEnumerable<Announcement>? announcements = await RetrieveDataFromRedis<IEnumerable<Announcement>>("Announcement_valid");

            if (announcements.IsNullOrEmpty())
            {
                IEnumerable<Announcement> retvAnnouncements = _db.Announcements
                            .Where(a => a.IsActive && a.StartTime <= DateTime.Now && a.EndTime >= DateTime.Now)
                            .OrderBy(a => a.Sequence)
                            .ToList();
                SaveDataToRedis("Announcement_valid", retvAnnouncements);
                return retvAnnouncements;
            }
            else
            {
                return announcements!;
            }
        }
    }
}
