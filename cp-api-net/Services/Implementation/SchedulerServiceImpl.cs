﻿using cp_api_net.Hubs;
using cp_api_net.Models;
using cp_api_net.Services.Interfaces;
using Microsoft.AspNetCore.SignalR;
using static cp_api_net.Models.SD;

namespace cp_api_net.Services.Implementation
{
    public class SchedulerServiceImpl : ISchedulerService
    {
        public readonly IHubContext<StreamingHub> _hubContext;

        public SchedulerServiceImpl(IHubContext<StreamingHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task SendMessageToCrypto()
        {
            Random _random = new Random();
            var ok = _random.Next(1000, 5000 + 1).ToString();
            await SendMessage(ok, StreamingType.DATA_CRYPTO);
        }

        public async Task SendMessageToEvent()
        {
            Random _random = new Random();
            var ok = _random.Next(1000, 5000 + 1).ToString();
            await SendMessage(ok, StreamingType.DATA_EVENT, "TEST");
        }

        public async Task SendMessageToStock()
        {
            Random _random = new Random();
            var ok = _random.Next(1000, 5000 + 1).ToString();
            await SendMessage(ok, StreamingType.DATA_STOCK);
        }

        private async Task SendMessage(string message, StreamingType streamingType, string agent = null)
        {
            if (streamingType.Equals(StreamingType.DATA_EVENT))
            {
                await _hubContext.Clients.Group($"{streamingType}_{agent}").SendAsync($"{streamingType}", message);
            }
            else
            {
                await _hubContext.Clients.All.SendAsync($"{streamingType}", message);
            }
        }
    }
}
