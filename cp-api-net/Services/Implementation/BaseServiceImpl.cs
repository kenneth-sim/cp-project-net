﻿using cp_api_net.Data;
using cp_api_net.Services.Interfaces;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace cp_api_net.Services.Implementation
{
    public class BaseServiceImpl : IBaseService
    {
        private readonly IDistributedCache _distributedCache;
        private readonly AppDbContext _db;

        public BaseServiceImpl(IDistributedCache distributedCache, AppDbContext db)
        {
            _distributedCache = distributedCache;
            _db = db;
        }

        public void RemoveFromRedis(string key)
        {
            _distributedCache.Remove(key);
        }

        public async Task<T?> RetrieveDataFromRedis<T>(string key) where T : class
        {
            string? cache = await _distributedCache.GetStringAsync(key);

            if (!string.IsNullOrEmpty(cache))
            {
                T data = JsonConvert.DeserializeObject<T>(cache)!;
                if (data != null)
                {
                    if (data is IEnumerable<Object>)
                    {
                        foreach (var item in (IEnumerable<Object>)data)
                        {
                            if (item is T typedItem)
                            {
                                _db.Set<T>().Attach(typedItem);
                            }
                        }
                    }
                    else
                    {
                        _db.Set<T>().Attach(data);
                    }

                    return data;
                }
            }

            return default;
        }

        public async void SaveDataToRedis(string key, object data, TimeSpan expirationTime = default)
        {
            if (expirationTime == default)
            {
                expirationTime = TimeSpan.FromMinutes(1);
            }

            await _distributedCache.SetStringAsync(key, JsonConvert.SerializeObject(data), new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = expirationTime
            });
        }
    }
}
