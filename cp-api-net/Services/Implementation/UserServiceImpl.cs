﻿using cp_api_net.Data;
using cp_api_net.Models.Dto;
using cp_api_net.Models.Entities;
using cp_api_net.Models.Error;
using cp_api_net.Services.Interfaces;

namespace cp_api_net.Services.Implementation
{
    public class UserServiceImpl : IUserService
    {
        private readonly AppDbContext _db;

        public UserServiceImpl(AppDbContext db)
        {
            _db = db;
        }

        public User GetUserByUserName(string userName)
        {
            var user = _db.Users.FirstOrDefault(u => u.UserName.Equals(userName));

            if (user == null)
            {
                throw new ErrorCodeException(ErrorCode.NOTFOUND_USER);
            }

            return user;
        }
    }
}
