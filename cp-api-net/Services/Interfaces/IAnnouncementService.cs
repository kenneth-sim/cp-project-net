﻿using cp_api_net.Models.Entities;

namespace cp_api_net.Services.Interfaces
{
    public interface IAnnouncementService
    {
        Task<IEnumerable<Announcement>> GetAnnouncements();
        void AddAnnouncement(Announcement newAnnouncement);
        void DeleteAnnouncement(int id);
        void UpdateAnnouncement(Announcement announcement);
        Task<IEnumerable<Announcement>> GetValidAnnouncements();
    }
}
