﻿namespace cp_api_net.Services.Interfaces
{
    public interface ISchedulerService
    {
        Task SendMessageToCrypto();
        Task SendMessageToStock();
        Task SendMessageToEvent
            ();
    }
}
