﻿namespace cp_api_net.Services.Interfaces
{
    public interface IBaseService
    {
        Task<T?> RetrieveDataFromRedis<T>(string key) where T : class;
        void SaveDataToRedis(string key, object data, TimeSpan expirationTime);
        void RemoveFromRedis(string key);
    }
}
