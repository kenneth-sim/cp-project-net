﻿using cp_api_net.Models.Entities;

namespace cp_api_net.Services.Interfaces
{
    public interface IUserService
    {
        User GetUserByUserName(String userName);
    }
}
