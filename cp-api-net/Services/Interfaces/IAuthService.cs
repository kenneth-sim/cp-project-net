﻿using AuthService.Models.Dto;
using cp_api_net.Models.Dto;

namespace cp_api_net.Services.Interfaces
{
    public interface IAuthService
    {
        Task Register(RegistrationRequest requestDto);
        Task<string> Login(LoginRequest requestDto);
        void Logout(string username);
        Task AssignRole(AssignRoleRequest assignRoleRequest);
    }
}
