﻿using cp_api_net.Models.Error;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Net;
using System.Net.NetworkInformation;

namespace cp_api_net.Models.Dto
{
    public class Response
    {
        public int StatusCode { get; set; }
        public string? Status { get; set; }
        public Object Data { get; set; }

        public Response(Object data) { 
            StatusCode = (int) HttpStatusCode.OK;
            Status = HttpStatusCode.OK.ToString();
            Data = data;
        }

        public Response(int statusCode, string status, Object data) { 
            StatusCode = statusCode;
            Status = status;
            Data = data;
        }

        public Response(ModelStateDictionary modelState)
        {
            StatusCode = (int) HttpStatusCode.BadRequest;
            Status = HttpStatusCode.BadRequest.ToString();
            Data = modelState.Keys
                .Select(key => ErrorCodeExtensions.GetValidationErrorCode(key))
                .Select(ec => new Response((int) ec.GetStatusCode(), ec.ToString(), ec.GetMessage()))
                .ToList();
        }
    }
}
