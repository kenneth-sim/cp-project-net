﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Net;

namespace cp_api_net.Models.Dto
{
    public class ValidationFailedResponse : ObjectResult
    {
        public ValidationFailedResponse(ModelStateDictionary modelState) : base(new Response(modelState))
        {
            StatusCode = (int) HttpStatusCode.BadRequest;
        }
    }
}
