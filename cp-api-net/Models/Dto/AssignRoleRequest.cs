﻿namespace cp_api_net.Models.Dto
{
    public class AssignRoleRequest
    {
        public string Username { get; set; }
        public string RoleName { get; set; }
    }
}
