﻿using System.ComponentModel.DataAnnotations;

namespace cp_api_net.Models.Entities
{
    public class Wallet
    {
        [Key]
        public int Id { get; set; }
        public decimal Balance { get; set; }
        public string Currency { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
    }
}
