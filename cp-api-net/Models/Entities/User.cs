﻿using Microsoft.AspNetCore.Identity;

namespace cp_api_net.Models.Entities
{
    public class User : IdentityUser
    {
        public string? Name { get; set; }
        public string? Jwt { get; set; }
        public string Agent {  get; set; }
        public DateTime? LastLoginTime { get; set; }
        public override string UserName { get => base.UserName!; set => base.UserName = value; }
        public ICollection<Order> Orders { get; set; }
        public ICollection<Wallet> Wallets { get; set; }
    }
}
