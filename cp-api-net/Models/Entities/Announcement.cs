﻿using cp_api_net.Services.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace cp_api_net.Models.Entities
{
    public class Announcement : IAuditableEntity
    {
        [Key]
        public int Id { get; set; }
        public int Sequence { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
