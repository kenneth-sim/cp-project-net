﻿using System.ComponentModel.DataAnnotations;

namespace cp_api_net.Models.Entities
{
    public class Game
    {
        [Key]
        public int Id { get; set; }
        public ICollection<GameSession> GameSessions { get; set; }
        public ICollection<GameResult> GameResults { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
