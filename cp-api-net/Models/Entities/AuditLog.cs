﻿using System.ComponentModel.DataAnnotations;

namespace cp_api_net.Models.Entities
{
    public class AuditLog
    {
        [Key]
        public int Id { get; set; }
        public string? Username { get; set; }
        public string RequestUrl { get; set; }
        public string Method { get; set; }
        public DateTime RequestTime { get; set; }
        public DateTime ResponseTime { get; set; }
        public string? RequestData { get; set; }
        public string? ResponseData { get; set; }
        public string ElapsedTime { get; set; }
        public string? Jwt { get; set; }
    }
}
