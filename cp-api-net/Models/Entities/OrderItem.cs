﻿using cp_api_net.Services.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace cp_api_net.Models.Entities
{
    public class OrderItem : IAuditableEntity
    {
        [Key]
        public int Id { get; set; }
        public decimal PlaceAmount { get; set; }
        public decimal WalletAmount { get; set; }
        public decimal ReturnAmount { get; set; }
        public string Status { get; set; }
        public string Snapshot { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
