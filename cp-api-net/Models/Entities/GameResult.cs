﻿using System.ComponentModel.DataAnnotations;

namespace cp_api_net.Models.Entities
{
    public class GameResult
    {
        [Key]
        public int Id { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}
