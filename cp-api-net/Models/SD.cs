﻿namespace cp_api_net.Models
{
    public class SD
    {
        public enum RoleType
        {
            ROLE_PLAYER,
            ROLE_MERCHANT,
            ROLE_ADMIN
        }

        public enum StreamingType
        {
            DATA_CRYPTO,
            DATA_STOCK,
            DATA_EVENT
        }

        public static bool IsValueInEnum<T>(string value) where T : struct
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enum type");
            }

            return Enum.IsDefined(typeof(T), value);
        }
    }
}
