# CP_API_NET

## 目录

- [简介](#简介)  
- [功能](#功能)  
- [技术](#技术)  
- [准备](#准备)  
- [维护](#维护)  
- [测试](#本地构建测试)
- [其它](#其它)  
- [文档]()

## 技术
该项目使用的是ASP.NET CORE 8.0, EF Core, Serilog, Hangfire, SignalR, ELK 等等<br/>
开发程序IDE建议使用 [Microsoft Visual Studio 2022 Community Edition](https://visualstudio.microsoft.com/downloads/), 并安装其自动建议的扩展包 <br/>
更多所需开发程序为:
1. SourceTree - 用于Git
2. SQL Server Management Studio 19 - 用于DB
3. Postman - 测试API
4. Notepad++, TortoiseGit, WinMerge - 可选下载，仅为简化开发

## 维护
关于class, method, variable 和 folder 的naming practice 请按照原有的风格      
folderName : PascalCase    
variable   : PascalCase (public) | CamelCase with prefix "_" (private)
className  : PascalCase 

## 本地构建测试
- 需要配置docker, redis
- 测试url: https://localhost:4001/swagger/index.html (触发启动按钮时一般自动启动)